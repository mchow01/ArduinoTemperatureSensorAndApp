//
//  main.m
//  TemperatureGraphApp-iOS
//
//  Created by Ming Chow on 6/4/14.
//  Copyright (c) 2014 Ming Chow. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
