//
//  AppDelegate.h
//  TemperatureGraphApp-iOS
//
//  Created by Ming Chow on 6/4/14.
//  Copyright (c) 2014 Ming Chow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
