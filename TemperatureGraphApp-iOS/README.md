#Building
Use core-plot as a static library (already included in app folder).  See https://github.com/core-plot/core-plot/wiki/Using-Core-Plot-in-an-Application for instructions.

#Screenshot
![iPhone Screenshot](screenshot.png "iPhone Screenshot")